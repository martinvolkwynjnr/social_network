const Hapi=require('hapi');
const server = new Hapi.Server();
const mongoose = require("mongoose");
const User = require("./databaseModels/userModel");

const socialNetworkDB = mongoose.connect("mongodb://localhost/socialNetworkDB");

//const server = new Hapi.Server({ port: 3000, host: 'localhost' });
// Create a server with a host and port

server.connection({port:3000});
server.start(console.log("test"));

//start();

// Add the route
server.route({
    method:'GET',
    path:'/',
    handler: function(request,reply) {
    	console.log(request);
    	console.log(reply);
    	reply.view("landingPage");
    }
});

const vision = require("vision");
//console.log(vision, __dirname);
const ejs = require("ejs");
//console.log(ejs);
server.register(vision,function(err){
	server.views({
		engines: {
			ejs: ejs
		},
		
		relativeTo: __dirname,
		path: "views"
	})
});

server.register(require("inert"), function(err){
	
});

server.register(require("hapi-auth-cookie"));

server.auth.strategy("cookiesStrategy","cookie",{
	cookie: "socialNetworkCookie",
	password: "apofjewoeiapofjewoeiapofjewoeiew",
	isSecure: false  // in real world https would be best practice thus should be true in practice
});

server.route({
	method: "GET",
	path: "/{param*}",
	handler: {
		directory: {
			path: "public"
		}
	}
});

server.register({
	register: require("./routes/user")
}, function(err){
	if(err){
		return;	
	}
	
}
);

server.register({
	register: require("./routes/userProfile")
}, function(err){
	if(err){
		return;	
	}
	
}
);



server.register({
	register: require("./routes/friends")
}, function(err){
	if(err){
		return;	
	}
	
}
);

server.register({
	register: require("./routes/home")
}, function(err){
	if(err){
		return;	
	}
	
}
);

