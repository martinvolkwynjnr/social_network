const mongoose = require("mongoose");
const shortid = require("shortid");


var userSchema = mongoose.Schema({
	userID: {type: String, default: shortid.generate},
	firstName: {type:String},
	surname: {type:String},
    email: {type: String},
	password: {type:String},
	friends: [{"userID": String,"email": String, "friendName": String, "friendLastName": String}],
	friendRequests: [{"userID": String,"email": String, "friendName": String, "friendLastName": String}]
});

var user = mongoose.model("User",userSchema);

module.exports = user;


