const User = require("../../databaseModels/userModel");

module.exports.register = function(server, options, next){
    server.route([
        {
          method: "POST",
          path: "/signUp",
          handler: function(request, reply)  {
        	  //register button clicked
        	  //findOne method provided by mongoose
        	  //test to check if email already exist
            User.findOne({"email": request.payload.email}, function(err,existing_user){
                if(existing_user){
                    reply("This email has already been registered. Try again with another email").code(400);
                } else{
                	var user = new User({"email": request.payload.email, "firstName": request.payload.firstName, "surname": request.payload.surname,"password": request.payload.password, "userProfile": {}}); 
                    user.save(function(err, savedUserRecord){
                        if(err){                        	
                            reply("Error during signing up").code(400);
                        } else{
                        	request.cookieAuth.set({"user": savedUserRecord.email, "memberID": savedUserRecord.memberID,"firstName":savedUserRecord.firstName});
                            reply("Successfully signed up");
                        }
                    });
                }
            });
          }
        },
        {
        	method: "POST",
        	path: "/login",
        	handler: function(request,reply){
        		//register button clicked
        		console.log("requestPayload", request.payload);
        		User.findOne({"email": request.payload.email,"password":request.payload.password}, function(err,validUser){
        			if(validUser){
        				request.cookieAuth.set({"user": validUser.email, "memberID": validUser.memberID,"firstName":validUser.firstName});
        				reply();
        			} else {
        				reply().code(400);
        			}
        		});
        	}
        },
        {
        	method: "POST",
        	path: "/logout",
        	handler: function(request,reply){
        		request.cookieAuth.clear();
        		reply();
        	}
        }

    ]);

    next();
};

module.exports.register.attributes = {
    pkg: require("./package.json")
};