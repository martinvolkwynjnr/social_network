var User =  require('../../databaseModels/userModel')


exports.register = function(plugin,options,next){
	plugin.route([
		{
			method: "GET",
			path: "/friends",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					
					
					
					/*User.findOne({"email": request.auth.credentials.user}, function(err,user){						
						reply.view("friends",{firstName: user.firstName,
												email: user.email,
												surname: user.surname,
											   friendRequests: user.friendRequests,
											   userFriends: user.friends,
											   memberID: request.auth.credentials.memberID
						});
						
					});*/
					console.log("getting latest changes");
					User.find({"email": {$ne: request.auth.credentials.user}},function(err,users){
						console.log(users);
						reply.view("friends",{userFriends: users});
						
					});
				}
			}
		},
		{
			method: "POST",
			path: "/search",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					User.find({"email": request.payload.search},function(err,users){
						console.log(users);
						reply(users);
						
					});
				}
			}
		},
		{
			method: "POST",
			path: "/friendRequest",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					 User.find({"email": request.auth.credentials.user}, function(err,sendingUser){
						 console.log("Sending user");
						 console.log(sendingUser);
						 console.log(request.payload.friendMemberID);
							User.find({"userID": request.payload.friendMemberID}, function(err,potentialFriend){
								
								console.log("Potential Friend");
								console.log(potentialFriend);
								potentialFriend[0].update({$push: {"friendRequests": {
									"userID": sendingUser[0].userID,
									"uniqueEmail": sendingUser[0].email,
									"friendName": sendingUser[0].firstName,
									"friendLastName": sendingUser[0].surname
									}}},function(err){
									reply('Friend Request Successfully Sent');
								});
							});
					});				  
				}
			}
		}

	]);
	next();
}

exports.register.attributes = {
	pkg: require("./package.json")
};