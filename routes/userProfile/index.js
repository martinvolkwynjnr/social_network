var fs = require('fs');
var User = require("../../databaseModels/userModel");
var shortid = require('shortid');

exports.register = function(plugin,option,next){
	plugin.route([
		{
			method: "GET",
			path: "/userProfile",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					User.findOne({"email": request.auth.credentials.user}, function(err,user){
						
						reply.view("userProfile",{firstName: user.firstName,
												email: user.email,
												surname: user.surname,
											   friendRequests: user.friendRequests,
											   userFriends: user.friends,
											   memberID: request.auth.credentials.memberID
						});
						
					});
				}
			}
		},
		{
			method: "POST",
			path: "/userProfile/edit",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					User.findOne({"email": request.auth.credentials.user}, function(err,user){
						user.firstName = request.payload.firstName
						user.surname = request.payload.surname
						user.email = request.payload.email
						user.save(function(err){
							if(err){
								reply("Err updating profile").code(400);
							} else{
								reply(user);
							}
						});
					});
				}
			}
		},
		{
			method: "POST",
			path: "/acceptFriendRequest",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					User.find({"email": request.auth.credentials.user}, function(err,user){
						console.log("userID",request.payload.userID)
						User.find({"userID": request.payload.userID}, function(err,acceptedFriendUser){
								user[0].update({$push: {"friends": {"email": acceptedFriendUser[0].email,"userID": acceptedFriendUser[0].userID,"friendName": acceptedFriendUser[0].firstName,"friendLastName": acceptedFriendUser[0].surname}},$pull: {"friendRequests": {userID: request.payload.userID}}},
												function(err){
													console.log("err",err)
												}
				
								);

								acceptedFriendUser[0].update({$push:{"friends": {"email": request.auth.credentials.user,"userID": user[0].userID,"friendName": user[0].firstName,"friendLastName": user[0].surname}}},
										function(err){
											console.log("err",err)
										}
								);

							reply('You have accepted a friend request');
						});
					});		  
				}
			}
		},
		{
			method: "POST",
			path: "/rejectFriendRequest",
			config: {
				auth: "cookiesStrategy",
				handler: function(request,reply){
					User.find({"email": request.auth.credentials.user}, function(err,user){
								user[0].update({$pull: {"friendRequests": {userID: request.payload.userID}}},
												function(err){
													reply(200)
												}
												
								);
					});
								  
				}
			}
		}

	])
	next();
}

exports.register.attributes = {
	pkg: require('./package.json')
};